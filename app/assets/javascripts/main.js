$(document).ready(function(){

	// Initialize autocomplete to search happy hours in any city
	var defaultBounds = new google.maps.LatLngBounds(
	      new google.maps.LatLng(-90,-180),
	      new google.maps.LatLng(90,180)
	);
	var options = {
		types: ['(cities)'],
	    bounds: defaultBounds
	};
	var citySearch = document.getElementById("city-search");
	var citySearchAutocomplete = new google.maps.places.Autocomplete(citySearch, options);
	citySearchAutocomplete.addListener('place_changed', function() {
		console.log("ciao");
	});

	// Initialize autocomplete in the modal to create new happy hours
	var defaultBounds = new google.maps.LatLngBounds(
		new google.maps.LatLng(-90,-180),
		new google.maps.LatLng(90,180)
	);
	var options = {
		bounds: defaultBounds
	};
	var selectPlace = document.getElementById("bar-input");
	var hpAutocomplete = new google.maps.places.Autocomplete(selectPlace, options);
	hpAutocomplete.addListener('place_changed', function() {
		var place = hpAutocomplete.getPlace();
		console.log(place);
		var phone_number = place.international_phone_number
		if (place.website){
			var website = place.website
		} else{
			var website = place.url
		}
		var image = place.photos[0].getUrl({maxWidth: 1260});
		console.log(image);
		var full_address = place.formatted_address
		var bar_name = place.name;
		var lat = place.geometry.location.lat();
		var lng = place.geometry.location.lng();

		$('#lng').val(lng);
		$('#lat').val(lat);
		$('#website').val(website);
		$('#image').val(image);
		$('#phone-number').val(phone_number);
		$("#bar-name").val(bar_name);
		$('#full-address').val(full_address)
	});

	// Search for happy hours around a certain city
	$('#city-search').on('change', function(){
		$.ajax({
		  url: "/happy_hours/search?city="+$('#city-search').val()
		});
	})

	// Dynamically add drink prices
	var clickCounter = 0;
	$(document.body).on('click', '.add-drink-btn' ,function(){
		if (clickCounter < 2){
			$(this).fadeOut('1000');
			$(this).parent().next().children().first().fadeOut('1000');
			$(".drink-wrapper").append($("#new-drink").html());
			clickCounter++
		} else {
			alert("You can't add more drinks")
		}
		//$('[data-toggle="tooltip"]').tooltip();
	});

	// Show info window when hovering over a card
	activateHover();

	// for(var i=0; i<$('.happy-hour-card').length; i++){
	// 	markerId = parseInt($($('.happy-hour-card')[i]).attr("id").replace("marker-",""))
	// 	console.log(markerId);
	// 	google.maps.event.trigger(marker_list[markerId], 'click');
	// }

	// Create new happy hour
	$('.add-happy-hour-btn').on('click', function(){
		$.ajax({
		  type: "POST",
		  url: "/happy_hours",
		  data: $('#add-new-happy-hour-form').serialize().replace("utf8=%E2%9C%93&",""),
		  success: function(data){
		  	if (data.status == "OK") location.reload();
		  }
		});
	})
});

function activateHover(){
		$('.happy-hour-card').hover(function(){
			console.log("ciao");
			markerId = parseInt($(this).attr("id").replace("marker-",""));
			google.maps.event.trigger(marker_list[markerId], 'click');
			// var pos = {
			//       lat: parseFloat($(this).attr("data-lat")),
			//       lng: parseFloat($(this).attr("data-lng"))
			//     };

			// map.setCenter();
		});
	}