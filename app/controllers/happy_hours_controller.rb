class HappyHoursController < ApplicationController
  def index
    @happy_hours = HappyHour.all  
  end

  def create
  	@happy_hour = HappyHour.new
  	@happy_hour.bar_name = params[:bar_name]
  	@happy_hour.full_address = params[:full_address]
  	@happy_hour.lat = params[:lat]
  	@happy_hour.lng = params[:lng]
  	@happy_hour.phone_number = params[:phone_number]
  	@happy_hour.website = params[:website]
    @happy_hour.time_start = params[:time_start]
    @happy_hour.time_end = params[:time_end]
    
    params[:happy_days].each do |day|
      @happy_hour.days.push(Day.find_by_name(day))
    end

    @happy_hour.days = @happy_hour.days.uniq
    
    if params[:drink_types] != []
      params[:drink_types].each_with_index do |drink, i|
        @happy_hour.specials.push(Special.create(:drink => params[:drink_types][i], :price => params[:drink_prices][i]))
      end
    end

    if params[:image] == ""
      @happy_hour.image = "https://farm5.staticflickr.com/4415/36468638990_1aa570f469_c.jpg"
    else
      @happy_hour.image = params[:image]
    end

    params[:happy_days].each do |day|
      @happy_hour.days.push(Day.find_by_name day)
    end

  	if @happy_hour.save
  	  respond_to do |format|
	      format.js { }
		    format.json {
		      render json: { status: "OK"}
		    }
	    end
    else
      respond_to do |format|
  	    format.js { }
    		format.json {
    		  render json: { status: "NOK"}
    		}
  	  end
    end
  end

  def search
    # @apartments = Apartment.filter_result(params[:city], params[:amenities], params[:wifi_speed], params[:avg_price], params[:min_stay], params[:bedrooms], params[:apartment_type])
    if params[:city] && params[:city] == "Chiang Mai Thailand"
      params[:city] = "MAYA Lifestyle Shopping Center Chang Phueak Chiang Mai Thailand"
    end

    @happy_hours = HappyHour.within(10, :origin => params[:city]).all if params[:city] != ""

    respond_to do |format|
        format.js { }
        format.json {render json: {
          :response => @apartments
        }}
    end
  end

end