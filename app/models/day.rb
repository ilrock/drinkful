class Day < ApplicationRecord
  has_many :happy_days 
  has_many :happy_hours, through: :happy_days
end
