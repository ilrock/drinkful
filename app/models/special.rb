class Special < ApplicationRecord
  belongs_to :happy_hour
  before_save :assign_icon

  def assign_icon
  	case self.drink
  	when "Beer"
  		self.icon = "<span class='ion-beer'></span>"
  	when "Wine"
  		self.icon = "<span class='ion-wineglass'></span>"
  	when "Cocktail"
  		self.icon = "<span class='ion-android-bar'></span>"
  	end
  end
end
