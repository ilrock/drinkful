class HappyHour < ApplicationRecord
  has_many :happy_days 
  has_many :days, -> { distinct }, through: :happy_days
  has_many :specials
  acts_as_mappable :default_units => :kms,
                   :lat_column_name => :lat,
                   :lng_column_name => :lng
end
