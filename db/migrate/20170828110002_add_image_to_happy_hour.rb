class AddImageToHappyHour < ActiveRecord::Migration[5.1]
  def change
  	add_column :happy_hours, :image, :string
  end
end
