class CreateHappyHours < ActiveRecord::Migration[5.1]
  def change
    create_table :happy_hours do |t|
      t.string :lat
      t.string :lng
      t.string :bar_name
      t.string :time_start
      t.string :time_end
      
      t.timestamps
    end
  end
end
