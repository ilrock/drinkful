class AddColumnToHappyday < ActiveRecord::Migration[5.1]
  def change 
  	add_column :happy_days, :day_id, :integer 
  	add_column :happy_days, :happy_hour_id, :integer 
  end
end
