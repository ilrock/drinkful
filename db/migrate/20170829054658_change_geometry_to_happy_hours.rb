class ChangeGeometryToHappyHours < ActiveRecord::Migration[5.1]
  def change
  	remove_column :happy_hours, :lat, :string
  	remove_column :happy_hours, :lng, :string

  	add_column :happy_hours, :lat, :float
  	add_column :happy_hours, :lng, :float
  end
end
