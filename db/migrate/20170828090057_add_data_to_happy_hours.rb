class AddDataToHappyHours < ActiveRecord::Migration[5.1]
  def change
  	add_column :happy_hours, :full_address, :string
  	add_column :happy_hours, :phone_number, :string
  	add_column :happy_hours, :website, :string
  end
end
