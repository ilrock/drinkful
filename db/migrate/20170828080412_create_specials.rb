class CreateSpecials < ActiveRecord::Migration[5.1]
  def change
    create_table :specials do |t|
      t.string :drink
      t.string :price
      t.string :icon
      t.timestamps
    end
  end
end
