Rails.application.routes.draw do
  root to: "happy_hours#index"
  post "/happy_hours", to: "happy_hours#create", :defaults => {:format => 'json'}
  get "/happy_hours/search", to: "happy_hours#search", :defaults => { :format => 'js' }
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
